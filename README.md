# Explorations kubernetes

[[_TOC_]]

Voici un petit descriptif de mes vagabondages dans le monde kubernetes.

Mon infrastructure est la suivante :

- Un cluster [OpenNebula](https://opennebula.io/)
- Un réseau privé `192.168.0.0/24`
- Une passerelle vers internet `192.168.0.1`
- Le [service kubernetes](https://marketplace.opennebula.io/appliance/547ecdff-f392-43b9-abc9-5f10a9fa7aff) déployé sur ce réseau

## Publication de services vers l’extérieur

Rendre accessible un service depuis l’extérieur du kubernetes nécessite l’utilisation de composants [ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/) :

```mermaid
flowchart LR
    subgraph kubernetes
        I[Ingress] -->|routing rule| S[Service]
        S --> P1[Pod]
        S --> P2[Pod]
	end
    C -->|1. Lookup name| DNS
    C([Client]) -->|Query public IP| I
    LB(LoadBalancer) -.->|provides public IPs| I
```

Pour résumer, le contrôleur `ingress` sert de reverse proxy HTTP/HTTPS pour vos applications.

Lors de mes premiers pas avec kubernetes, j’ai eu de grandes séances d’accélérations calviciesques en ne trouvant pas la réponse à la question : « mais comment je fais pour accéder à mon application depuis mon poste ».

Le problème vient que tous les exemples que l’on trouve sur internet considèrent que vous êtes en infrastructure géré par un hébergeur :

1. Vous achetez votre infrastructure kubernetes chez un hébergeur
2. Vous déployez un contrôleur `ingress`
3. Votre hébergeur attribue automatiquement une adresse IP à au contrôleur `ingress` déployé
4. Vous configurer des noms de dommaine associés à cette adresse IP
5. Vous activez le TLS (généralement Let's Encrypt de façon automatique)
6. Vous déployez des applications dans votre kubernetes en définissant les [règles ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/#ingress-rules) pour diriger les requêtes arrivant sur l’adresse IP publique que votre hébergeur vous a fourni vers les pod qui font tourner l’application

Ainsi, dans mon infrastructure, l’étape `3.` n’est pas réalisée car il faut un composant spécial le `LoadBalancer`.

### Le `LoadBalancer`

Le schéma montre qu’en amont de l’`ingress`, il y a un système appelé [`LoadBalancer`](https://kubernetes.io/docs/tasks/access-application-cluster/create-external-load-balancer/), un répartiteur de charge.

Comme indiqué dans la [documentation](https://kubernetes.io/docs/tasks/access-application-cluster/create-external-load-balancer/), ce `LoadBalancer` est un composant externe au kubernetes, son rôle est de distribuer aux contrôleurs `ingress` que vous installez des adresses IP externes (`EXTERNAL-IP` dans la sortie de la commande `kubectl get svc`) accessibles de l’extérieur de votre kubernetes.

Les infrastructures kubernetes que vous louez auprès d’un héberger sont fournies avec le composant kubernetes permettant de configurer automatiquement les services de type `LoadBalancer`. Ce composant interagi avec le `LoadBalancer` pour récupérer une adresse IP externe atteignable par les utilisateurs.

### Le DNS

Ce vénérable service d’internet est externe à votre kubernetes, enfin il y en a un en interne mais à l’usage unique de kubernetes. Pour configurer les noms de domaines publiques où sont exposées vos applications, vous devez configurer les enregistrements manuellement ou alors mettre en œuvre [ExternalDNS](https://github.com/kubernetes-sigs/external-dns).

## Un `LoadBalancer` interne à kubernetes

Il est possible de palier au problème du `LoadBalancer` en déployant [kube-vip](https://kube-vip.io/):

1. Vous devez avoir un pool d’adresse IP non utilisé, dans mon cas, j’ai [réservé une IP dans OpenNebula](http://docs.opennebula.io/5.12/operation/network_management/manage_vnets.html#hold-and-release-leases) afin qu’elle ne soit pas attribuer à une machine virtuelle
2. J’ai déployé kube-vip [selon la documentation](https://github.com/plunder-app/kube-vip/blob/master/docs/kubernetes/index.md), une seule IP pour un seul espace de nommage
3. J’ai déployé [traefik](https://traefik.io/) avec `helm`
4. J’ai déployé l’application [`traefik/whoami`](https://github.com/traefik/whoami) avec plusieurs `pod` et une règle `ingress` qui l’associe à `traefik`

Lorsque je me connecte de HTTP sur l’adresse IP réservée, l’application [`traefik/whoami`](https://github.com/traefik/whoami) répond et me donne les informations me permettant de vérifier la répartition des requêtes sur les différents `pods`.


## Empaquetage HELM de `kube-vip`

Étant donné que ce service sera d’une forte utilité pour toute personne n’ayant pas d’infrastructure chez un hébergeur, je vais tenter d’en faire un paquet [helm](https://helm.sh).

Pour commencer, je récuppère les [fichiers](https://kube-vip.io/manifests/) pour déployer `kube-vip` :

<details>
<summary>controller.yaml</summary>

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: plunder-cloud-controller
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  annotations:
    rbac.authorization.kubernetes.io/autoupdate: "true"
  name: system:plunder-cloud-controller-role
rules:
  - apiGroups: [""]
    resources: ["configmaps", "endpoints","events","services/status"]
    verbs: ["*"]
  - apiGroups: [""]
    resources: ["nodes", "services"]
    verbs: ["list","get","watch","update"]
---
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: system:plunder-cloud-controller-binding
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: system:plunder-cloud-controller-role
subjects:
- kind: ServiceAccount
  name: plunder-cloud-controller
  namespace: kube-system
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: plndr-cloud-provider
  namespace: kube-system
spec:
  serviceName: plndr-cloud-provider
  podManagementPolicy: OrderedReady
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      app: kube-vip
      component: plndr-cloud-provider
  template:
    metadata:
      labels:
        app: kube-vip
        component: plndr-cloud-provider
    spec:
      containers:
      - command:
        - /plndr-cloud-provider
        - --leader-elect-resource-name=plndr-cloud-controller
        image: plndr/plndr-cloud-provider:0.1.5
        name: plndr-cloud-provider
        imagePullPolicy: Always
        resources: {}
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
      serviceAccountName: plunder-cloud-controller
```
</details>

<details>
<summary>kube-vip.yaml</summary>

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: vip
---
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: vip-role
rules:
  - apiGroups: ["coordination.k8s.io"]
    resources: ["leases"]
    verbs: ["get", "create", "update", "list", "put"]
  - apiGroups: [""]
    resources: ["configmaps", "endpoints"]
    verbs: ["watch", "get"]
---
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: vip-role-bind
subjects:
  - kind: ServiceAccount
    name: vip 
    apiGroup: ""
roleRef:
  kind: Role
  name: vip-role
  apiGroup: "" 
---
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: kube-vip-cluster
  name: kube-vip-cluster
spec:
  replicas: 3
  selector:
    matchLabels:
      app: kube-vip-cluster
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: kube-vip-cluster
    spec:
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
                - key: "app"
                  operator: In
                  values:
                  - kube-vip-cluster
            topologyKey: "kubernetes.io/hostname"
      containers:
      - image: plndr/kube-vip:0.1.3
        imagePullPolicy: Always
        name: kube-vip
        command:
        - /kube-vip
        - service
        env:
          - name: vip_interface
            value: "eth0"
          - name: vip_configmap
            value: "plndr" 
          - name: vip_arp
            value: "true"
          - name: vip_loglevel
            value: "5"
        resources: {}
        securityContext:
          capabilities:
            add:
            - NET_ADMIN
      hostNetwork: true
      serviceAccountName: vip
status: {}
```
</details>

On commence par créer un modèle avec la commande `helm create kube-vip`

### Déclaration du paquet : le fichier `Chart.yaml`

<details>
<summary>kube-vip/Charts.yaml</summary>

```yaml
apiVersion: v2
name: kube-vip
description: High-Availability and load-balancing for both inside and outside a Kubernetes cluster
type: application
version: 0.1.0
appVersion: "0.3.2"

icon: https://raw.githubusercontent.com/plunder-app/kube-vip/v0.3.2/docs/index/kube-vip.png
home: https://kube-vip.io/
maintainers:
  - name: EOLE team
    email: eole-team@ac-dijon.fr
    url: https://github.com/eole
```
</details>

### Le déploiement de `kube-vip`

Certains attributs sont en dur car obligatoires :

- `hostNetwork` et `securityContext` du pod pour permettre au `pod` d’écouter sur l’interface du nœud
- `affinity` du `pod` pour s’assurer que tous les réplicas ne sont pas sur le même nœud

<details>
<summary>kube-vip/template/deployment.yaml</summary>

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "kube-vip.fullname" . }}
  labels:
    {{- include "kube-vip.labels" . | nindent 4 }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "kube-vip.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "kube-vip.selectorLabels" . | nindent 8 }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "kube-vip.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      hostNetwork: true
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            capabilities:
              add:
                - NET_ADMIN
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          command:
            - /kube-vip
            - service
          env:
            {{- toYaml .Values.env | nindent 12 }}
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            - labelSelector:
                matchLabels:
                  {{- include "kube-vip.selectorLabels" . | nindent 18 }}
              topologyKey: "kubernetes.io/hostname"
      {{- with .Values.affinity }}
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
```
</details>
